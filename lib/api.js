//api.js
const config = require('../config')

const api = {
  //house
  getHouses: ({data, success}) => get('/house/index', data, success),

  //good
  createGood: ({data, success}) => post('/good/create', data, success)
}

function get(url, data = {}, success = ()=>{}) {
  var method = "GET"
  var token = wx.getStorageSync('token')
  data['token'] = token
  url = config.host + url
  wx.request({url, data, method, success: res => {
    middleware(res, success)
  }})
}

function post(url, data = {}, success = ()=>{}) {
  var method = "POST"
  var token = wx.getStorageSync('token')
  data['token'] = token
  url = config.host + url
  wx.request({url, data, method, success: res => {
    middleware(res, success)
  }})
}

function middleware(res, next) {
  //一些中间处理
  next(res)
}

module.exports = api
