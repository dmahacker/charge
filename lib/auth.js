//auth.js
const config = require('../config')

function login() {
  wx.showLoading({ title: '登陆中...' })
  var token = wx.getStorageSync('token')
  if(token) {
    //检测token是否有效
    wx.request({
      url: `${config.host}/check-token`,
      method: 'POST',
      data: { token: token },
      success: res => {
        var data = res.data
        if(data.code != config.response.SUCCESS) {
          refreshToken()
        } else {
          getUserInfo(token)
          wx.hideLoading()
        }
      }
    })
  } else {
    refreshToken()
  }
}

function refreshToken() {
  wx.login({
    success: res => {
      wx.request({
        url: `${config.host}/login`,
        method: 'POST',
        data: { code: res.code },
        success: res => {
          var data = res.data
          wx.hideLoading()
          if(data.code != config.response.SUCCESS) {
            wx.showModal({
              title: '登陆错误',
              content: data.data.errcode + ":" + data.msg,
              showCancel: false
            })
          } else {
            getUserInfo(data.data)
            wx.setStorage({ 'key': 'token', 'data': data.data })
            wx.hideLoading()
          }
        }
      })
    },
    fail: res => {
      wx.showToast({ title: '登陆失败' })
    }
  })
}

function getUserInfo(token) {
  var app = getApp()
  app.globalData.token = token
  wx.getUserInfo({
    success: userInfo => {
      var user = JSON.parse(userInfo.rawData)
      if(app.setUser) {
        app.setUser(user)
      }
      app.globalData.user = user
      console.log(user)
    }
  })
}

module.exports.login = login
