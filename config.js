//config.js

const config = {
  host: 'http://127.0.0.1/api',
  response: {
    SUCCESS: '1000',
    LOGIN_FAIL: '1001',
    TOKEN_INVALID: '1002',
    FORM_INVALID: '1003',
  }
}

module.exports = config
