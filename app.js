//app.js
const auth = require('./lib/auth')

App({
  onLaunch: () => {
    auth.login()
  },
  globalData: {
    token: '',
    user: {},
  }
})
