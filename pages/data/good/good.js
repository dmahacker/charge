//good.js
const config = require('../../../config')
const api = require('../../../lib/api')

Page({
  data: {
    houses: [{
      id: 0,
      name: '新建',
    }],
    form: {
      houseName: ''
    },
    houseIndex: 0,
  },
  onLoad() {
    api.getHouses({
      success: res => {
        var data = res.data
        if(data.code == config.response.SUCCESS) {
          var newHouses = data.data.concat(this.data.houses)
          this.setData({
            houses: newHouses
          })
        } else {
          console.log(res)
        }
      }
    })
  },
  setForm(name, key) {
    var form = this.data.form
    form[name] = key
    this.setData({ form })
  },
  houseChange(e) {
    var houseIndex = e.detail.value
    var houseId = this.data.houses[houseIndex]
    this.setForm('houseId', houseId)
    this.setData({ houseIndex })
  },
  houseInput(e) {
    var houseName = e.detail.value
    this.setForm('houseName', houseName)
  }
})
